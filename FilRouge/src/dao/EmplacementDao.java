package dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import model.Emplacement;
import servlet.HibernateUtil;

public class EmplacementDao implements IEmplacementDao {

	@Override
	public List<Emplacement> getListEmplacements() {
		Session session = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Emplacement> criteria = criteriaBuilder.createQuery(Emplacement.class);
		Root<Emplacement> root = criteria.from(Emplacement.class);
		criteria.select(root);
		Query<Emplacement> query = session.createQuery(criteria);
		List<Emplacement> resultList = query.getResultList();

		if (resultList.size() > 0) {
			return resultList;
		}
		return null;
	}

	@Override
	public Emplacement getEmplacementByID(long id) {
		Session session = HibernateUtil.getInstance();
		Emplacement emplacement = session.load(Emplacement.class, id);
		return emplacement;
	}

	@Override
	public void deleteEmplacement(Emplacement emplacement) {
		Session session = HibernateUtil.getInstance();
		Transaction transaction = session.beginTransaction();
		session.delete(emplacement);
		transaction.commit();

	}

	@Override
	public boolean createEmplacement(Emplacement emplacement) {
		Session hibernateSession = HibernateUtil.getInstance();
		Transaction transaction = hibernateSession.beginTransaction();
		try {
			hibernateSession.save(emplacement);
		} catch (Exception e) {
			return false;
		}
		transaction.commit();
		return true;
	}

	@Override
	public boolean updateEmplacement(Emplacement emplacement) {
		Session hibernateSession = HibernateUtil.getInstance();
		Transaction transaction = hibernateSession.beginTransaction();
		try {
			hibernateSession.update(emplacement);
		} catch (Exception e) {
			return false;
		}
		transaction.commit();
		return true;
	}

}
