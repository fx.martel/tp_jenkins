package dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import model.Camping;
import servlet.HibernateUtil;

public class CampingDao implements ICampingDao {

	@Override
	public List<Camping> getListCampings() {
		Session session = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Camping> criteria = criteriaBuilder.createQuery(Camping.class);
		Root<Camping> root = criteria.from(Camping.class);
		criteria.select(root);
		Query<Camping> query = session.createQuery(criteria);
		List<Camping> resultList = query.getResultList();

		if (resultList.size() > 0) {
			return resultList;
		}
		return null;
	}

	@Override
	public List<Camping> getListCampingsbyRegion(String region) {
		Session session = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Camping> criteria = criteriaBuilder.createQuery(Camping.class);
		Root<Camping> root = criteria.from(Camping.class);
		criteria.where(criteriaBuilder.equal(root.get("region"), region));
		Query<Camping> query = session.createQuery(criteria);
		List<Camping> resultList = query.getResultList();

		if (resultList.size() > 0) {
			return resultList;
		}
		return null;
	}

	@Override
	public Camping getCampingByID(long id) {
		Session session = HibernateUtil.getInstance();
		Camping camping = session.load(Camping.class, id);
		return camping;
	}

	@Override
	public void deleteCamping(Camping camping) {
		Session session = HibernateUtil.getInstance();
		Transaction transaction = session.beginTransaction();
		session.delete(camping);
		transaction.commit();

	}

	@Override
	public boolean createCamping(Camping camping) {
		Session hibernateSession = HibernateUtil.getInstance();
		Transaction transaction = hibernateSession.beginTransaction();
		try {
			hibernateSession.save(camping);
		} catch (Exception e) {
			return false;
		}
		transaction.commit();
		return true;
	}

	@Override
	public boolean updateCamping(Camping camping) {
		Session hibernateSession = HibernateUtil.getInstance();
		Transaction transaction = hibernateSession.beginTransaction();
		try {
			hibernateSession.update(camping);
		} catch (Exception e) {
			return false;
		}
		transaction.commit();
		return true;
	}

}
