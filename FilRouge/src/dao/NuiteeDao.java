package dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import model.Nuitee;
import servlet.HibernateUtil;

public class NuiteeDao implements INuiteeDao {

	@Override
	public List<Nuitee> getListNuitees() {
		Session session = HibernateUtil.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Nuitee> criteria = criteriaBuilder.createQuery(Nuitee.class);
		Root<Nuitee> root = criteria.from(Nuitee.class);
		criteria.select(root);
		Query<Nuitee> query = session.createQuery(criteria);
		List<Nuitee> resultList = query.getResultList();

		if (resultList.size() > 0) {
			return resultList;
		}
		return null;
	}

	@Override
	public Nuitee getNuiteeByID(long id) {
		Session session = HibernateUtil.getInstance();
		Nuitee nuitee = session.load(Nuitee.class, id);
		return nuitee;
	}

	@Override
	public void deleteNuitee(Nuitee nuitee) {
		Session session = HibernateUtil.getInstance();
		Transaction transaction = session.beginTransaction();
		session.delete(nuitee);
		transaction.commit();

	}

	@Override
	public boolean createNuitee(Nuitee nuitee) {
		Session hibernateSession = HibernateUtil.getInstance();
		Transaction transaction = hibernateSession.beginTransaction();
		try {
			hibernateSession.save(nuitee);
		} catch (Exception e) {
			return false;
		}
		transaction.commit();
		return true;
	}

	@Override
	public boolean updateNuitee(Nuitee nuitee) {
		Session hibernateSession = HibernateUtil.getInstance();
		Transaction transaction = hibernateSession.beginTransaction();
		try {
			hibernateSession.update(nuitee);
		} catch (Exception e) {
			return false;
		}
		transaction.commit();
		return true;
	}

}
