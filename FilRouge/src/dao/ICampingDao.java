package dao;

import java.util.List;

import model.Camping;

public interface ICampingDao {

	public Camping getCampingByID(long id);

	public List<Camping> getListCampings();

	public List<Camping> getListCampingsbyRegion(String region);

	public void deleteCamping(Camping camping);

	public boolean createCamping(Camping camping);

	public boolean updateCamping(Camping camping);

}
