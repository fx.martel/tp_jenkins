package dao;

import java.util.List;

import model.Emplacement;

public interface IEmplacementDao {

	public Emplacement getEmplacementByID(long id);

	public List<Emplacement> getListEmplacements();

	public void deleteEmplacement(Emplacement emplacement);

	public boolean createEmplacement(Emplacement emplacement);

	public boolean updateEmplacement(Emplacement emplacement);

}
