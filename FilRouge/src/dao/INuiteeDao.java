package dao;

import java.util.List;

import model.Nuitee;

public interface INuiteeDao {

	public Nuitee getNuiteeByID(long id);

	public List<Nuitee> getListNuitees();

	public void deleteNuitee(Nuitee nuitee);

	public boolean createNuitee(Nuitee nuitee);

	public boolean updateNuitee(Nuitee nuitee);

}
