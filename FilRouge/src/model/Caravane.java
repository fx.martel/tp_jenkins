package model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Caravane extends Emplacement {

	public Caravane() {
		prix = 13.5F;
	}

}
