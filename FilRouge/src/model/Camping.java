package model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Camping {

	@Id
	@GeneratedValue
	private int id;
	private String nom;
	private String gerant;
	private String adresse;
	private String region;
	private Date ouverture;
	private Date fermeture;

	@OneToMany(mappedBy = "camping")
	private List<Emplacement> emplacements;

	public Camping() {
		super();
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getGerant() {
		return gerant;
	}

	public void setGerant(String gerant) {
		this.gerant = gerant;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Date getOuverture() {
		return ouverture;
	}

	public void setOuverture(Date ouverture) {
		this.ouverture = ouverture;
	}

	public Date getFermeture() {
		return fermeture;
	}

	public void setFermeture(Date fermeture) {
		this.fermeture = fermeture;
	}

	public List<Emplacement> getEmplacements() {
		return emplacements;
	}

	public void setEmplacements(List<Emplacement> emplacements) {
		this.emplacements = emplacements;
	}

	public int getNbEmplacements() {
		if (getEmplacements() == null) {
			return 0;
		} else {
			return getEmplacements().size();
		}

	}
}
