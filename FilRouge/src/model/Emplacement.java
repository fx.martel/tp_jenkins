package model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@DiscriminatorColumn(name="prix", 
//discriminatorType = DiscriminatorType.FLOAT)
public abstract class Emplacement {
	@Id
	@GeneratedValue
	private int id;

	@ManyToOne
	private Camping camping;

	private int surface;
	private int capacite;
	protected float prix;

	@OneToMany(mappedBy = "emplacement")
	private List<Nuitee> nuitees;

	private int numero;

	public Emplacement() {
		super();
	}

	public int getSurface() {
		return surface;
	}

	public void setSurface(int surface) {
		this.surface = surface;
	}

	public int getCapacite() {
		return capacite;
	}

	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	public Camping getCamping() {
		return camping;
	}

	public void setCamping(Camping camping) {
		this.camping = camping;
	}

	public List<Nuitee> getNuitees() {
		return nuitees;
	}

	public void setNuitees(List<Nuitee> nuitees) {
		this.nuitees = nuitees;
	}

}
