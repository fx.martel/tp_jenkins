package model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class CampingCar extends Emplacement {

	public CampingCar() {
		prix = 14F;
	}

}
