package model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Bungalow extends Emplacement {

	public Bungalow() {
		prix = 17.5F;
	}

}
