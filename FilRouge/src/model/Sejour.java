package model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class Sejour {
	@Id
	@GeneratedValue
	private int id;

	@OneToOne
	private Groupe groupe;

	@OneToMany(mappedBy = "sejour")
	private List<Nuitee> nuitees;

	@OneToMany(mappedBy = "sejour")
	private List<Activite> activites;

	@OneToOne
	private Facture facture;

	public Sejour() {
		super();
	}

	public Groupe getGroupe() {
		return groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

	public List<Nuitee> getNuitees() {
		return nuitees;
	}

	public void setNuitees(List<Nuitee> nuitees) {
		this.nuitees = nuitees;
	}

	public List<Activite> getActivites() {
		return activites;
	}

	public void setActivites(List<Activite> activites) {
		this.activites = activites;
	}

	public Facture getFacture() {
		return facture;
	}

	public void setFacture(Facture facture) {
		this.facture = facture;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
