package servlet;

import java.util.List;

import org.hibernate.Session;

import dao.CampingDao;
import model.Camping;

public class Test {
	public static void main(String[] args) {
		Session session = new HibernateUtil().getInstance();
		List<Camping> listCampings = new CampingDao().getListCampings();
		for (Camping camping : listCampings) {
			System.out.println("Camping : " + camping.getNom() + ", r�gion : " + camping.getRegion() + ", adresse : "
					+ camping.getAdresse());
		}
	}
}
