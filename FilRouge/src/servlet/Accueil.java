package servlet;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CampingDao;
import model.Camping;
import model.Emplacement;
import model.Nuitee;

public class Accueil extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Camping> listeCampings = new CampingDao().getListCampings();
		HashSet<String> setRegions = new HashSet<String>();
		for (Camping camping : listeCampings) {
			setRegions.add(camping.getRegion());
		}
		req.setAttribute("dateJour", LocalDate.now());
		req.setAttribute("setRegions", setRegions);
		req.getRequestDispatcher("/WEB-INF/JSP_Files/accueil.jsp").forward(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int personnes = Integer.parseInt(req.getParameter("personnes"));
		int nuitees = Integer.parseInt(req.getParameter("nbNuitees"));
		Date dateDebut = Date.valueOf(req.getParameter("dateDebut"));

		String region = req.getParameter("region");
		req.getSession().setAttribute("personnes", personnes);
		req.getSession().setAttribute("region", region);
		req.getSession().setAttribute("dateDebut", dateDebut);
		req.getSession().setAttribute("nbNuitees", nuitees);

		List<Camping> listeCampings = filtrerListeCampingParDispoSurDateEtCapacite(
				new CampingDao().getListCampingsbyRegion(region), dateDebut, nuitees, personnes);

		req.setAttribute("listeCampings", listeCampings);
		req.getRequestDispatcher("/WEB-INF/JSP_Files/listeCampings.jsp").forward(req, resp);
	}

	public static List<Camping> filtrerListeCampingParDispoSurDateEtCapacite(List<Camping> listeCampings,
			Date dateDebut, int nbNuitees, int personnes) {

		Calendar c = Calendar.getInstance();
		c.setTime(dateDebut);
		c.add(Calendar.DATE, nbNuitees);
		java.util.Date dateFin = c.getTime();

		for (Camping camping : listeCampings) {
			List<Emplacement> emplacements = camping.getEmplacements();
			for (Emplacement emplacement : emplacements) {
				if (emplacement.getCapacite() >= personnes) {
					List<Nuitee> nuitees = emplacement.getNuitees();
					for (Nuitee nuitee : nuitees) {
						if (nuitee.getDate().getTime() >= dateDebut.getTime()
								&& nuitee.getDate().getTime() <= dateFin.getTime()) {
							emplacements.remove(emplacement);
							break;
						}
					}
				} else {
					emplacements.remove(emplacement);
				}
			}
			camping.setEmplacements(emplacements);
			if (emplacements.size() == 0) {
				listeCampings.remove(camping);
			}
		}
		return listeCampings;
	}

}
