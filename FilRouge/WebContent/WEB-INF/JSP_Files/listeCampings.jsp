<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" xml:lang="en-gb"
	lang="en-gb">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<head>
<meta charset="UTF-8">
	<link rel="stylesheet"
		href="//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
		<link rel="stylesheet"
			href="//cdn.rawgit.com/necolas/normalize.css/master/normalize.css">
			<link rel="stylesheet"
				href="//cdn.rawgit.com/milligram/milligram/master/dist/milligram.min.css">

				<marquee>Voici les campings disponibles le ${dateDebut}
				pour ${nbNuitees} nuit�es</marquee>
</head>
<body class="ltr">

	<div id="wrapcentre">

		<div id="pagecontent">


			<br clear="all" />

			<table class="tablebg" cellspacing="1" width="100%">
				<tbody>

					<tr>
						<th>&nbsp;Nom&nbsp;</th>
						<th>&nbsp;R�gion&nbsp;</th>
						<th>&nbsp;Adresse&nbsp;</th>
						<th>&nbsp;G�rant&nbsp;</th>
						<th>&nbsp;Emplacements&nbsp;</th>
						<th>&nbsp;Emplacements&nbsp;disponibles&nbsp;le&nbsp;${dateDebut}&nbsp;pour&nbsp;${nbNuitees}&nbsp;nuit�es&nbsp;</th>
						<th>&nbsp;Date&nbsp;d'ouverture&nbsp;</th>
						<th>&nbsp;Date&nbsp;de&nbsp;fermeture&nbsp;</th>
					</tr>

					<c:forEach items="${listeCampings}" var="camping">
						<tr>
							<td class="row1" align="center" width="20%">${camping.nom}</td>
							<td class="row2" align="center" width="10%">${camping.region}</td>
							<td class="row1" align="center" width="15%">${camping.adresse}</td>
							<td class="row2" align="center" width="10%">${camping.gerant}</td>
							<td class="row1" align="center" width="5%">${camping.nbEmplacements}</td>
							<td class="row2" align="center" width="30%"><select
								name="emplacementCamping">
									<c:forEach items="${camping.emplacements}" var="emplacement">
										<!--  							<option value="${emplacement.id}">n�${emplacement.id}, capacit� : ${emplacement.capacite}, surface : ${emplacement.surface}, prix : ${emplacement.prix}</option></c:forEach>
-->
										<option value="${emplacement.id}">${emplacement.prix}</option>
									</c:forEach>
							</select></td>
							<td class="row1" align="center" width="5%">${camping.ouverture}</td>
							<td class="row2" align="center" width="5%">${camping.fermeture}</td>

						</tr>
					</c:forEach>

				</tbody>
			</table>
			<br clear="all" />
		</div>

		<br clear="all" />
	</div>

</body>
</html>
