<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" xml:lang="en-gb"
	lang="en-gb">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<head>
<meta charset="UTF-8">
	<link rel="stylesheet"
		href="//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
		<link rel="stylesheet"
			href="//cdn.rawgit.com/necolas/normalize.css/master/normalize.css">
			<link rel="stylesheet"
				href="//cdn.rawgit.com/milligram/milligram/master/dist/milligram.min.css">
				<title>Bienvenue au campigne de la Cerisaie</title>
</head>

<body>
	<marquee>Le groupe des campings de la Cerisaie vous souhaite la bienvenue</marquee>
	<div>
		<form action="index" method="post">
			 <label>� combien eud'personnes euq'vous venez ?</label>
			 <input name="personnes" type="number" min="1" max="12" required></input><br>
			 <label>O� qu'c'est qu'tu veux passer tes vacances ?</label>
			 <select name="region">
					<c:forEach items="${ setRegions }" var="region">
					<option value="${region}">${region}</option>
					</c:forEach>
			</select><br>
			<label>quand qu'c'est qu'tu voudrais partir ?</label>
			<input name="dateDebut" type="date" required min="${dateJour }" placeholder="${dateJour }"></input><br>
			<label>Combien eud'nuits qu'tu voudrais rester au campigne ?</label>
			<input name="nbNuitees" type="number" required></input><br>
			<label>Dans quoi qu'tu veux loger ?</label>
			<input name="typeEmplacement" type="radio" value="Tente" required></input><br>
			<input name="typeEmplacement" type="radio" value="Caravane" required></input><br>
			<input name="typeEmplacement" type="radio" value="CampingCar" required></input><br>
			<input name="typeEmplacement" type="radio" value="Bungalow" required></input><br>
			
			<input type="submit" value="cherche un campigne o� qu'y a d'la place"></input>
		</form>
	</div>


</body>
</html>